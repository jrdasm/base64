package main

import "errors"
import "flag"
import "fmt"
import "io"
import "os"
import "strconv"
import "strings"

const (
	usage = `Usage: base64 [OPTION]... [FILE]
Base64 encode or decode FILE, or standard input, to standard output.

With no FILE, or when FILE is -, read standard input.

Mandatory arguments to long options are mandatory for short options too.
  -d, --decode          decode data
  -i, --ignore-garbage  when decoding, ignore non-alphabet characters
  -w, --wrap=COLS       wrap encoded lines after COLS character (default 76).
                          Use 0 to disable line wrapping
      --help     display this help and exit
      --version  output version information and exit

The data are encoded as described for the base64 alphabet in RFC 4648.
When decoding, the input may contain newlines in addition to the bytes of
the formal base64 alphabet.  Use --ignore-garbage to attempt to recover
from any other non-alphabet bytes in the encoded stream.

GNU coreutils online help: <https://www.gnu.org/software/coreutils/>
Report any translation bugs to <https://translationproject.org/team/>
Full documentation <https://www.gnu.org/software/coreutils/base64>
or available locally via: info '(coreutils) base64 invocation'
`
	DEFAULT_WRAP      = 76
	BYTE_BITS_GROUP   = 8
	B64_BITS_GROUP    = 6
	ALPHABET          = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
	PADDING           = '='
	MIN_BITS          = 24                         // 8 × 3 = 6 × 4
	MIN_BYTES         = MIN_BITS / BYTE_BITS_GROUP // 3
	ENCODE_GROUP_SIZE = MIN_BITS / B64_BITS_GROUP  // 4
	ENCODE_SHIFT      = MIN_BITS - B64_BITS_GROUP
	ENCODE_MASK       = ((1 << B64_BITS_GROUP) - 1) << ENCODE_SHIFT
)

func main() {
	version := [...]int{1, 0, 0}
	flag.CommandLine.SetOutput(os.Stdout)
	flag.Usage = func() { fmt.Fprintf(os.Stdout, "%s", usage) }
	var (
		version_flag        bool
		decode_flag         bool
		ignore_garbage_flag bool
		wrap_flag           int
	)
	flag.BoolVar(&version_flag, "version", false, "output version information and exit")
	flag.BoolVar(&decode_flag, "d", false, "decode data")
	flag.BoolVar(&decode_flag, "decode", false, "decode data")
	flag.BoolVar(&ignore_garbage_flag, "i", false, "when decoding, ignore non-alphabet characters")
	flag.BoolVar(&ignore_garbage_flag, "ignore-garbage", false, "when decoding, ignore non-alphabet characters")
	flag.IntVar(&wrap_flag, "w", DEFAULT_WRAP, "wrap encoded lines after COLS character")
	flag.IntVar(&wrap_flag, "wrap", DEFAULT_WRAP, "wrap encoded lines after COLS character")
	flag.Parse()
	if version_flag {
		var version_str []string
		for _, v := range version {
			version_str = append(version_str, strconv.Itoa(v))
		}
		fmt.Println(strings.Join(version_str, "."))
		os.Exit(0)
	}
	var f *os.File
	if flag.NArg() == 0 || flag.Arg(0) == "-" {
		f = os.Stdin
	} else {
		var err error
		f, err = os.Open(flag.Arg(0))
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}
		defer f.Close()
	}
	var err error
	if decode_flag {
		err = decode(f, ignore_garbage_flag)
	} else {
		err = encode(f, wrap_flag)
	}
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func encode(f *os.File, wrap_flag int) error {
	var (
		err         error
		nb_bytes    int
		bytes_array []byte
	)
	defer os.Stdout.Sync()
	bytes_array = make([]byte, MIN_BYTES)
	nb_bytes, err = f.Read(bytes_array)
	if err == io.EOF {
		err = nil
	}
	alphabet := []rune(ALPHABET)
	padding := 0
	n := 0
	for err == nil && nb_bytes != 0 && padding == 0 {
		e := 0
		for i := 0; i < MIN_BYTES; i++ {
			e <<= BYTE_BITS_GROUP
			if i < nb_bytes {
				e += int(bytes_array[i])
			}
		}
		padding := MIN_BYTES - nb_bytes
		encoded := ""
		for i := 0; i < ENCODE_GROUP_SIZE; i++ {
			if ENCODE_GROUP_SIZE-padding > i {
				idx := (e & ENCODE_MASK) >> ENCODE_SHIFT
				e <<= B64_BITS_GROUP
				encoded += string(alphabet[idx])
			} else {
				encoded += string(PADDING)
			}
			n += 1
			if wrap_flag != 0 && n == wrap_flag {
				encoded += "\n"
				n = 0
			}
		}
		os.Stdout.WriteString(encoded)
		bytes_array = make([]byte, MIN_BYTES)
		nb_bytes, err = f.Read(bytes_array)
		if err == io.EOF {
			err = nil
		}
	}
	if n != 0 {
		os.Stdout.WriteString("\n")
	}
	return err
}

func decode(f *os.File, ignore_garbage_flag bool) error {
	var (
		err         error
		nb_bytes    int
		bytes_array []byte
	)
	defer os.Stdout.Sync()
	bytes_array = make([]byte, ENCODE_GROUP_SIZE)
	nb_bytes, err = f.Read(bytes_array)
	if err == io.EOF {
		err = nil
	}
encode_loop:
	for err == nil && nb_bytes != 0 {
		e := 0
		n := 0
		for i := 0; i < ENCODE_GROUP_SIZE; i++ {
			e <<= B64_BITS_GROUP
			for i < nb_bytes {
				char := bytes_array[i]
				if idx := strings.IndexByte(ALPHABET, char); idx != -1 {
					e += idx
					n += 1
					break
				} else if char == PADDING {
					n += 1
					break
				} else if strings.IndexByte("\n\r", char) != -1 || ignore_garbage_flag {
					// read next char
					for j := i; j < ENCODE_GROUP_SIZE-1; j++ {
						bytes_array[j] = bytes_array[j+1]
					}
					new_byte := make([]byte, 1)
					nb, _ := f.Read(new_byte)
					if nb > 0 {
						bytes_array[ENCODE_GROUP_SIZE-1] = new_byte[0]
					} else {
						nb_bytes -= 1
					}
				} else {
					err = errors.New("base64: invalid input")
					break encode_loop
				}
			}
		}
		if n < ENCODE_GROUP_SIZE {
			err = errors.New("base64: invalid input")
			break encode_loop
		}
		decoded := make([]byte, ENCODE_GROUP_SIZE)
		for i := ENCODE_GROUP_SIZE - 1; i >= 0; i-- {
			b := byte(e) // only lower bits
			decoded[i] = b
			e >>= BYTE_BITS_GROUP
		}
		os.Stdout.Write(decoded)
		bytes_array = make([]byte, ENCODE_GROUP_SIZE)
		nb_bytes, err = f.Read(bytes_array)
		if err == io.EOF {
			err = nil
		}
	}
	return nil
}
